# coinmarketcap.com/cryptocurrency-category/

from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json

url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/categories'
parameters = {
  'start':'1',
  'limit':'5000'
}
headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': 'f20643e9-9a43-4eee-9b7b-8ce6fcdbb346',
}

session = Session()
session.headers.update(headers)

try:
  response = session.get(url, params=parameters)
  data = json.loads(response.text)
  print(data)
except (ConnectionError, Timeout, TooManyRedirects) as e:
  print(e)